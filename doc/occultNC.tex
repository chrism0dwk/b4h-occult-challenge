\documentclass[a4paper]{article}

\usepackage[margin=1in]{geometry}
\usepackage{bm}
\usepackage{float}
\usepackage{amsmath}
\usepackage{natbib}
\usepackage{tikz}
\usetikzlibrary{positioning,calc,fit,arrows,backgrounds}


\setlength{\parskip}{2ex}
\setlength{\parindent}{0ex}

\title{Exploration of discrete state spaces for partially observed linear state transition models.}

\author{Chris Jewell}


\begin{document}
	\maketitle
	
	\section{Motivating example: Epidemics}
	
	In an epidemic model, individuals are assumed to move between a number of mutually exclusive disease states according to transition rates which may be dependent on the states of other individuals in the system at any one time.  Figure \ref{fig:sir} depicts the well known ``SIR'' model in which individuals start out as \emph{Susceptible}, become \emph{Infected} with the disease, and finally \emph{Recover} from the disease and play no further part in the epidemic.  
	
	As described in \citet{JewEtAl09c}, the transition rate $\lambda(t)$ from \emph{Susceptible} to \emph{Infected} depends on the current number of individuals in the \emph{Infected} state, whilst the transition $\gamma$ from \emph{Infected} to \emph{Removed} is constant with respect to time.  In general, we model transition rates at the individual level such that
	\begin{equation}
	\lambda_j(t) = s(j; x_j, \xi) \sum_{i \in \mathcal{I}(t)} K(i, j, \psi)q(i; x_i,\zeta) \label{eq:lambda}
	\end{equation}
	is the transition rate for individual $j$ from \emph{Susceptible} to \emph{Infected} at time $t$ where $\mathcal{I}(t)$ is the set of infected individuals at time $t$, $s(j; x_j, \xi)$ is the susceptibility of $j$ conditional on covariates $x_j$, $q(i; x_i, zeta)$ is the infectivity of $i$ conditional on covariates $x_i$, and $K(i,j, \psi)$ is a kernel determining connectivity between $i$ and $j$. 
	
	N.B. $\lambda_j(t)$ reduces to the \emph{homogeneously-mixing} SIR model if $s(j; x_j, \xi) = q(i; x_i, zeta) = 1$ and $K(i, j, \psi) = \beta$ for all $i \in \mathcal{I}(t)$ and $j\in \mathcal{S}(t)$ at time $t$.
	
		\begin{figure}
		\centering
		\begin{tikzpicture}[
		auto,
		node distance=40pt,
		box/.style={
			draw=black,
			anchor=center,
			align=center,
			minimum height=40pt,
			minimum width=40pt},
		bCircle/.style={
			circle,
			draw=black,
			anchor=center,
			align=center,
			minimum height=40pt,
			minimum width=40pt}]
		\node[box] (S) {$\mathcal{S}$};
		\node[box, right=of S] (I) {$\mathcal{I}$};
		\node[box, right=of I] (R) {$\mathcal{R}$};
		
		\draw[->,very thick] (S) to node (SI) {$\lambda(t)$} (I);
		\draw[->,very thick] (I) to node (IR) {$\gamma$} (R);
		\draw[-*,dashed] (I) to [out=240,in=270,looseness=2] (SI);
		\end{tikzpicture}
		\caption{\label{fig:sir}The canonical ``SIR'' model showing states \emph{Susceptible} ($\mathcal{S}$), \emph{Infected} ($\mathcal{I}$), and \emph{Recovered} ($\mathcal{R}$)}
	\end{figure}

	
	\section{Data generating model}\label{sec:data-generating-model}
	
	In terms of a stochastic data generating model, the SIR state transition model may be set up as a Markov process where the occurrence of transition events depend \emph{only} on the state of the epidemic immediately after the previous event.  Denoting Exponentially-distributed event times for individuals as $Y^{k}_j, \; k\in\{SI, IR\}$, we have
	\begin{eqnarray}
	Pr(t < Y^k_j < t+s | Y^k_j > t) &=& \lambda_j(t) + o(s), \;\; k = SI \nonumber \\
	Pr(t < Y^k_j < t+s | Y^k_j > t) &=& \gamma + o(s), \; \; k = IR \nonumber
	\end{eqnarray} 
	
	Given a particular starting state, $\mathcal{Y}_0 = \{\mathcal{S}(0), \mathcal{I}(0),\mathcal{R}(0)\}$, the epidemic system may be propagated forward in continuous time using the Doob-Gillespie exact algorithm, or in discrete time using Euler-Maruyama approximations such as the Chain Binomial algorithm.
	
	\section{Inference}\label{sec:inference}
	In an operational setting, we are interested in estimating parameters $\bm{\theta} = \{\xi, \zeta, \psi, \mbox{etc.}\}$ given observations of the events occurring in the epidemic timeseries.  
	
	The Markov setup of the SIR model described in Section \ref{sec:data-generating-model} allows a straightforward construction of a time-inhomogeneous (marked) Poisson process likelihood function, conditional on transition times, as previously described in \citet{NealRob04} and \citet{JewEtAl09c}. For example, for the continuous time setup, we have
	
	\begin{equation}
	L(Y^{SI}, Y^{IR} | \mathcal{Y}_0, \bm{theta}) = \prod_{j: y^{SI}_j > 0} \left[ \lambda_j(y^{SI-}_j) e^{-\int_0^\infty \sum_{j} \lambda_j(t) \mathrm{d}t} \right] \prod_{j:y_j^{IR}<\infty} \left[ \gamma e^{-\gamma(y^{IR}_j - y^{SI}_j)} \right] \label{eq:likelihood}
	\end{equation}
	which immediately makes conditioning on event times apparent.
	
	
	From Equation \ref{eq:likelihood}, it is apparent that inference is straightforward -- maximise the likelihood over the parameters or apply priors and draw posterior samples using MCMC.  In practice, however, $SI$ transitions are \emph{unobserved} which poses two technical challenges:
	
	\begin{enumerate}
		\item The definition of the likelihood function in Equation \ref{eq:likelihood} is intractable to analytic integration with respect to the infection times $\bm{y}^{SI}$;
		\item As illustrated by Figure \ref{fig:sir_occults}, if the epidemic is observed part way through, there may be a large disparity between the \emph{true} number of \emph{Infected} individuals and the apparent number of infected individuals calculated through observation of individuals undergoing the $IR$ transition. These so-called \textbf{occult} infections must be explored in order to provide unbiased estimation of $\theta$.
	\end{enumerate}

	One way (the ``gold standard'' way) of dealing with challenge (1) is to employ MCMC data augmentation techniques, where the \emph{unobserved} data $\bm{y}^{SI}$ are treated as parameters to be estimated.  
	
	For challenge (2), the current data augmentation MCMC methods proposed in \citet{NealRob04} and \citet{JewEtAl09c} employ component-wise jump algorithms. Briefly, considering an epidemic timeseries up to $t_{obs}$, for individuals $j$ where $y^{IR}_j > t_{obs}$ (i.e. those for whom a removal time has not yet been observed), we propose to add $(j^\star, y^{SI}_{j^\star}$ to the parameter space.  A corresponding reverse proposal selects a previously added $y^{SI}_{j^\star}$ to remove from the parameter space.


	\section{Challenge}
		
	The current component-wise jump procedure for adding/removing occult infections is known to mix slowly for situations in which the missing data space is potentially large -- exploring a large space component-wise is simply too slow in MCMC iteration terms.
	
	This case is well exemplified in \citet{JewEtAl2009a} in which real-time model-fitting was used to track the 2007 UK foot and mouth disease outbreak, now-casting the spatial extent of occult infections (where farms were classed as individuals).  Here, occult exploration was improved by preferentially proposing $j^\star$ close to the main (spatial) focus of the outbreak.  This improved MCMC mixing for individuals nearby at the expense of even poorer mixing for individuals further away.  This model-driven method of exploring the occult space is clearly undesirable, particularly if the model turns out to be wrong!
	
	\textbf{Proposal}:  to solve the ``occult problem'', a way of jointly updating the occult space would be desirable where $\bm{j}^\star$ is chosen reflecting the dependence inherent in the epidemic process.  A potential way of doing this is to non-centre all infection times using the ``Sellke construction'' described in an epidemic context by \citet{AnBr00}.  Here, each individual (whether observed or not) receives a ``threshold value'' $$u_j \overset{iid}{\sim} \mbox{Exp}(1)$$ decoupling \emph{a priori} an individual's propensity to become infected with the epidemic process itself.  Such a set up may be more amenable to wide-scale multisite updating using Metropolis-Hastings than the conventional (centred) parameterisation of the model.

\begin{figure}
	\centering
	\includegraphics[width=0.66\textwidth]{sir_occults.pdf}
	\caption{\label{fig:sir_occults}A realisation of a stochastic SIR epidemic trajectory, showing number of Infected and Removed individuals.  At $t=25$, we might suppose the number of infected individuals to be equal to the number of removals, when in fact it is much higher.}
\end{figure}


\bibliography{general}
\bibliographystyle{unsrtnat}

	
\end{document}